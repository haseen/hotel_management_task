import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DoubleroomTest {
    @org.junit.Test
   public void CheckingCustomerDetails() {
        Doubleroom obj = new Doubleroom("haseen", "8179710121", "male", "hasan", "9191919191", "male");
        assertEquals("haseen", obj.name);
        assertEquals("8179710121", obj.contact);
        assertEquals("male", obj.gender);
        assertEquals("hasan", obj.name2);
        assertEquals("9191919191", obj.contact2);
        assertEquals("male", obj.gender2);
    }

    @Test
    void CheckingIfDetailsAreNotNull() {
        Doubleroom obj = new Doubleroom("haseen", "8179710121", "male", "hasan", "9191919191", "male");
        assertNotNull(obj.name);
        assertNotNull(obj.contact);
        assertNotNull(obj.gender);
        assertNotNull(obj.name2);
        assertNotNull(obj.contact2);
        assertNotNull(obj.gender2);
    }

    @Test
    void CheckingIfDetailsAreNull() {
        Doubleroom obj = new Doubleroom(null, null, null, null, null, null);
        assertNull(obj.name);
        assertNull(obj.contact);
        assertNull(obj.gender);
        assertNull(obj.name2);
        assertNull(obj.contact2);
        assertNull(obj.gender2);
    }

    @Test
    void CheckingIfDetailsAreNotEqual() {
        Doubleroom obj = new Doubleroom("haseen", "8179710121", "male", "hasan", "9191919191", "male");
        assertNotEquals("alice", obj.name);
        assertNotEquals("8179710122", obj.contact);
        assertNotEquals("female", obj.gender);
        assertNotEquals("rachel", obj.name2);
        assertNotEquals("9191919192", obj.contact2);
        assertNotEquals("female", obj.gender2);
    }

    @Test
    void CheckingIfDetailsAreTrue() {
        Doubleroom obj = new Doubleroom("haseen", "8179710121", "male", "hasan", "9191919191", "male");
        assertTrue("haseen" == obj.name);
        assertTrue("8179710121" == obj.contact);
        assertTrue("male" == obj.gender);
        assertTrue("hasan" == obj.name2);
        assertTrue("9191919191" == obj.contact2);
        assertTrue("male" == obj.gender2);
    }

    @Test

    void CheckingIfDetailsAreFalse(){
        Doubleroom obj = new Doubleroom("haseen", "8179710121", "male", "hasan", "9191919191", "male");
        assertFalse("alexa"== obj.name);
        assertFalse("8179710124"== obj.contact);
        assertFalse("female"== obj.gender);
        assertFalse("Juni"== obj.name2);
        assertFalse("9191919190"==obj.contact2);
        assertFalse("female"== obj.gender2);
    }
}