import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class holderTest {
    @Test
    public void CheckingRoomSize() {
        holder obj = new holder();
        int luxury_doubleRoomSize = 10;
        int deluxe_doubleRoomSize = 20;
        int luxury_SingleRoomSize = 10;
        int deluxe_SingleRoomSize = 20;
        assertEquals(luxury_doubleRoomSize, obj.luxury_doublerrom.length);
        assertEquals(deluxe_doubleRoomSize, obj.deluxe_doublerrom.length);
        assertEquals(luxury_SingleRoomSize, obj.luxury_singleerrom.length);
        assertEquals(deluxe_SingleRoomSize, obj.deluxe_singleerrom.length);
    }

}