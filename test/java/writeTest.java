import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import static org.junit.jupiter.api.Assertions.*;

public class writeTest {

    @Test
    public void WritingAllCustomerDetails() {
        Hotel.bookroom(1);
        try {
            File f = new File("/home/dell/IdeaProjects/Hotel_management/src/test/java/backup");
            if (f.exists()) {
                FileInputStream fin = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fin);
                Hotel.hotel_ob = (holder) ois.readObject();
            }
        } catch (Exception e) {
            System.out.println("Not a valid input");
        }
        Thread t = new Thread(new write(Hotel.hotel_ob));
        t.start();
        assertEquals(9, Hotel.availability(1));
        assertNotNull(Hotel.hotel_ob.luxury_doublerrom[0]);
        //  System.out.println((Hotel.hotel_ob.luxury_doublerrom[0].name));
    }

}