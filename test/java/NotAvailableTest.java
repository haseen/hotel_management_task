import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NotAvailableTest {
    @org.junit.Test
    public void CheckRoomIfNotAvailable() {
        NotAvailable obj = new NotAvailable();
        String expectedOutput = "Not Available !";
        assertEquals(expectedOutput, obj.toString());
    }

    @Test
    void CheckIfNotNull() {
        NotAvailable obj = new NotAvailable();
        assertNotNull(obj.toString());
    }

}