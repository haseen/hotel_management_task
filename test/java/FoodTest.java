import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FoodTest {
    @org.junit.Test
    public void DataIsEqual() {
        Food obj = new Food(1, 1);
        Float price = obj.price;
        assertEquals(50, price, "should be float value");
    }

    @Test
    void DataIsNotNull() {
        Food obj = new Food(1, 1);
        Float price = obj.price;
        assertNotNull(obj.price);
    }

    @Test
    void DataIsNull() {
        Food obj = new Food(1, 1);
        Float price = obj.price;
        price = null;
        assertNull(price);
    }

    @Test
    void DataIsNotEquals() {
        Food obj = new Food(1, 1);
        Float price = obj.price;
        assertNotEquals(590, obj.price);
    }

    @Test
    void DataIsTrue() {
        Food obj = new Food(1, 1);
        assertTrue(50 == obj.price);
    }

    @Test
    void DataIsFalse() {
        Food obj = new Food(1, 2);
        assertFalse(50 == obj.price);
    }


}