import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class HotelTest {
    @Test
    public void SingleRoomCustomerDetails() {
        Hotel.CustDetails(3, 1);
        assertEquals("haseen", Hotel.hotel_ob.luxury_singleerrom[1].name);
        assertEquals("123", Hotel.hotel_ob.luxury_singleerrom[1].contact);
        assertEquals("male", Hotel.hotel_ob.luxury_singleerrom[1].gender);
        Hotel.CustDetails(4, 1);
        assertEquals("hasan", Hotel.hotel_ob.deluxe_singleerrom[1].name);
        assertEquals("321", Hotel.hotel_ob.deluxe_singleerrom[1].contact);
        assertEquals("male", Hotel.hotel_ob.deluxe_singleerrom[1].gender);
    }

    @Test

    public void DoubleRoomCustomerDetails() {
        Hotel.CustDetails(1, 1);
        assertEquals("harshit", Hotel.hotel_ob.luxury_doublerrom[1].name);
        assertEquals("345", Hotel.hotel_ob.luxury_doublerrom[1].contact);
        assertEquals("male", Hotel.hotel_ob.luxury_doublerrom[1].gender);
        assertEquals("charu", Hotel.hotel_ob.luxury_doublerrom[1].name2);
        assertEquals("567", Hotel.hotel_ob.luxury_doublerrom[1].contact2);
        assertEquals("male", Hotel.hotel_ob.luxury_doublerrom[1].gender2);
        Hotel.CustDetails(2, 1);
        assertEquals("mehta", Hotel.hotel_ob.deluxe_doublerrom[1].name);
        assertEquals("375", Hotel.hotel_ob.deluxe_doublerrom[1].contact);
        assertEquals("male", Hotel.hotel_ob.deluxe_doublerrom[1].gender);
        assertEquals("sohel", Hotel.hotel_ob.deluxe_doublerrom[1].name2);
        assertEquals("569", Hotel.hotel_ob.deluxe_doublerrom[1].contact2);
        assertEquals("male", Hotel.hotel_ob.deluxe_doublerrom[1].gender2);
    }

    @Test
    public void FeaturesOfLuxuryDoubleRoom() {
        Hotel.features(1);
        String str = "Number of double beds : 1\nAC : Yes\nFree breakfast : Yes\nCharge per day:4000 ";
        assertEquals(str, Hotel.features(1));
    }

    @Test
    public void FeaturesOfDeluxeDoubleRoom() {
        Hotel.features(2);
        String str = "Number of double beds : 1\nAC : No\nFree breakfast : Yes\nCharge per day:3000  ";
        assertEquals(str, Hotel.features(2));
    }

    @Test
    public void FeaturesOfLuxurySingleRoom() {
        Hotel.features(3);
        String str = "Number of single beds : 1\nAC : Yes\nFree breakfast : Yes\nCharge per day:2200  ";
        assertEquals(str, Hotel.features(3));
    }

    @Test
    public void FeaturesOfDeluxeSingleRoom() {
        Hotel.features(4);
        String str = "Number of single beds : 1\nAC : No\nFree breakfast : Yes\nCharge per day:1200 ";
        assertEquals(str, Hotel.features(4));
    }

    @Test

    public void LuxuryDoubleRoomsAvailableAfterBooking() {
        Hotel.bookroom(1);
        int roomBooked = Hotel.availability(1);
        assertEquals(9, roomBooked);
    }

    @Test
    public void CheckingDeluxeDoubleRoomsAvailableAfterBooking() {
        Hotel.bookroom(2);
        int roomBooked = Hotel.availability(2);
        assertEquals(19, roomBooked);
    }

    @Test
    public void CheckingLuxurySingleRoomsAvailableAfterBooking() {
        Hotel.bookroom(3);
        int roomBooked = Hotel.availability(3);
        assertEquals(9, roomBooked);
    }

    @Test
    public void CheckingDeluxeSingleRoomsAvailableAfterBooking() {
        Hotel.bookroom(4);
        int roomBooked = Hotel.availability(4);
        assertEquals(19, roomBooked);
    }

    //
//
//
    @Test
    public void OrderingFood() {
        int itemN = 0;
        int quantity = 0;
        int price = 0;
        Hotel.bookroom(1);
        Hotel.order(0, 1);
        //System.out.println(( Hotel.hotel_ob.luxury_doublerrom[0].food));
        for (Food v : Hotel.hotel_ob.luxury_doublerrom[0].food) {
            itemN += v.itemno;
            quantity += v.quantity;
            price += v.price;
        }
        System.out.println(itemN + "," + quantity + "," + price);
        assertEquals(1, itemN);
        assertEquals(1, quantity);
        assertEquals(50, price);
    }

    //
    @Test

    public void CheckOut() {
        Hotel.bookroom(1);
        Hotel.deallocate(0, 1);
        assertNull(Hotel.hotel_ob.luxury_doublerrom[0]);
    }

    @Test
    public void FinalBill() {
        double luxuryDoubleRoomCharge = 4000;
        Hotel.bookroom(1);
        Hotel.order(0, 1);
        for (Food v : Hotel.hotel_ob.luxury_doublerrom[0].food) {
            luxuryDoubleRoomCharge += v.price;
        }
        Hotel.deallocate(0, 1);
//        System.out.println((Hotel.hotel_ob.luxury_doublerrom[0].name));


        assertEquals(4050, luxuryDoubleRoomCharge); // luxuryDoubleRoomCharge(4000) + sandwich(50)

    }


}