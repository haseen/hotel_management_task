import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SingleroomTest {
    @org.junit.Test
    public void CheckingCustomerDetails() {
        Singleroom obj = new Singleroom("haseen", "8179710121", "male");
        assertEquals("haseen", obj.name, "Customer details didn't match");
        assertEquals("8179710121", obj.contact, "Customer details didn't match");
        assertEquals("male", obj.gender, "Customer details  didn't match");
    }

    @Test
    void CheckingIfDetailsAreNotNull() {
        Singleroom obj = new Singleroom("haseen", "8179710121", "male");
        assertNotNull(obj.name, "The details are null");
        assertNotNull(obj.contact, "The details are null");
        assertNotNull(obj.gender, "The details are null");
    }

    @Test
    void CheckingIfDetailsAreNull() {
        Singleroom obj = new Singleroom(null, null, null);
        assertNull(obj.name, "This Field has some value");
        assertNull(obj.contact, "This Field has some value");
        assertNull(obj.gender, "This Field has some value");
    }

    @Test
    void CheckingIfDetailsAreNotEqual() {
        Singleroom obj = new Singleroom("haseen", "8179710121", "male");
        assertNotEquals("alexa", obj.name, "The details are same");
        assertNotEquals("8179710122", obj.contact, "The details are same");
        assertNotEquals("female", obj.gender, "The details are same");
    }

    @Test
    void CheckingIfDetailsAreTrue() {
        Singleroom obj = new Singleroom("haseen", "8179710121", "male");
        assertTrue("haseen" == obj.name);
        assertTrue("8179710121" == obj.contact);
        assertTrue("male" == obj.gender);
    }

    @Test
    void CheckingIfDetailsAreFalse() {
        Singleroom obj = new Singleroom("haseen", "8179710121", "male");
        assertFalse("alexa" == obj.name);
        assertFalse("8179710122" == obj.contact);
        assertFalse("female" == obj.gender);
    }


}